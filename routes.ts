export const publicRoutes = ['/de', '/en']

export const authRoutes = ['/auth/login']

export const errorRoutes = ['/en/auth/error', '/en/auth/verify']

//prefix for api authentication routes: Routes that start with that prefix are used for api authentication purposes
export const apiAuthPrefix = '/api/auth'

export const DEFAULT_LOGIN_REDIRECT = '/browse'
